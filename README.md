# using-qc-tests-for-dast_nuclei_proxify_selenium

Earlier in a dummy CI/CD pipeline, I implemented a free DAST using python Selenium Scripts, similar to those used by QC teams, and proxied the Selenium scripts traffic to feed it into OWASP ZAP. With the new info and context, ZAP was used as a DAST to scan the newly pushed code.

In this repo, I am attempting to do the same. In a CI/CD pipeline, I am proxying the Python Selenium scripts, similar to those used by QC teams, into Project Discovery proxify tool. The requests and responses are saved. The requests and responses are fed into the OSS nuclei. Nuclei is a web application vulnerability scanner used by many web pen testers.