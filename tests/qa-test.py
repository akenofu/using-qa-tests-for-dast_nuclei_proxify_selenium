import os 
import time
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.proxy import Proxy, ProxyType


SELENIUM_URL = "chrome:4444"
QA_HOST = os.environ['QA_HOST']
QA_PORT = os.environ['QA_WEBSERVER_PORT']
target = 'http://' + QA_HOST + ":" + QA_PORT

proxy_host = '127.0.0.1'
proxy_port = '8888'

proxy = Proxy()
proxy.proxy_type = ProxyType.MANUAL
proxy.http_proxy = f"{proxy_host}:{proxy_port}"
proxy.ssl_proxy = f"{proxy_host}:{proxy_port}"

# Create desired capabilities object and pass proxy settings
desired_capabilities = webdriver.DesiredCapabilities.CHROME.copy()
proxy.add_to_capabilities(desired_capabilities)

browser = webdriver.Remote(
  f"http://{SELENIUM_URL}/wd/hub",
  desired_capabilities=desired_capabilities
)

browser.get(target)

print(f"Retrieved URL: {browser.current_url}.")
print(f"Retrieved Page Title: {browser.title}.")

try:
    # signup
    browser.get(target + '/taskManager/register/')
    time.sleep(2)

    # enter the username, password, firstname,lastname etc.
    browser.find_element(By.ID, 'id_username').send_keys('user10')

    time.sleep(1)
    browser.find_element(By.ID, 'id_first_name').send_keys('user')

    time.sleep(1)
    browser.find_element(By.ID, 'id_last_name').send_keys('user')

    time.sleep(1)
    browser.find_element(By.ID, 'id_email').send_keys('user@user1.com')

    time.sleep(1)
    browser.find_element(By.ID, 'id_password').send_keys('user123')

    time.sleep(1)
    submit = browser.find_element("css selector", '.btn.btn-danger').click()

    time.sleep(2)

    # login
    browser.get(target + '/taskManager/login/')
    browser.find_element(By.ID, 'username').send_keys('user10')
    browser.find_element(By.NAME, "password").send_keys('user123')
    submit = browser.find_element(By.XPATH, "//button[@type='submit']")
    submit.click()

    time.sleep(2)

    # spider the URL's
    browser.get(target + "/taskManager/dashboard/")
    time.sleep(2)

    browser.get(target + "/taskManager/task_list/")
    time.sleep(2)

    browser.get(target + "/taskManager/project_list/")
    time.sleep(2)

    browser.get(target + "/taskManager/search/")
    time.sleep(2)

    # editing some data for better scan results
    browser.get(target + "/taskManager/profile/")
    time.sleep(5)
    browser.find_element(By.NAME, "first_name").send_keys('firstroot')
    browser.find_element(By.NAME, 'last_name').send_keys('lastroot')
    browser.find_element(By.XPATH, "//button[@type='submit']").click()

    time.sleep(2)
finally:
    browser.close()

